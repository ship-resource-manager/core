# SRM Core

This component contains essential parts of the system that are necessary for the development of any script. These are:
- Key system tools, such as Clock or Storage.
- Abstractions above elements.
- Basic events.
